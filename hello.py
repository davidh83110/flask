from flask import Flask
import requests
import random
from bs4 import BeautifulSoup


app = Flask(__name__)

@app.route('/')
def hello_world():
    r = requests.get('http://www.google.com/')
    return 'Hello World!'+r.text

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
