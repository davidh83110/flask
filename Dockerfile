FROM 990090895087.dkr.ecr.ap-southeast-1.amazonaws.com/david-flask:latest


COPY requirements.txt /tmp/
COPY hello.py /tmp/hello.py

RUN pip install --requirement /tmp/requirements.txt

EXPOSE 8080

CMD exec python3 /tmp/hello.py
